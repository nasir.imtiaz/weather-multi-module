# Weather Forecast 

> A maven multi-module spring boot application for weather forecast.

### Features
  - Multi module with separate responsibility for each module
  - Periodic weather forecast updates (configurable)
  - Displaying weather forecast updates

###### Modules
- weather-multi-module
    - domain
    - persistence
    - web

### How to run:
  - clone this project into your local directory: 
  `git clone https://gitlab.com/nasir.imtiaz/weather-multi-module.git`
  - Build the project: 
  `mvn clean install package`
  (This will generate required artifacts to run the application.)
  - Change directory to `/weather-multi-module/web/target`
  - In order to run the generated executable jar, execute command `java -Dfile.encoding=utf-8 -jar web-0.0.1-SNAPSHOT.jar`
  > At this point the periodic weather forecasts will be retrieved from the given web service URL `http://www.ilmateenistus.ee/ilma_andmed/xml/forecast.php?lang=eng` and stored in the database.
  - Enter http://localhost:8080 in browser address bar to view the weather forecast updates visually