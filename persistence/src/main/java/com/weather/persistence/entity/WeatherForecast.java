package com.weather.persistence.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class WeatherForecast {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private int tempMin;
	private int tempMax;
	@Column(length=500)
	private String peipsi;
	@Column(length=500)
	private String text;
	@ManyToOne
	private Forecast forecast;
	@ManyToOne
	private ForecastTime forecastTime;
	@ManyToOne
	private Phenomenon phenomenon;
	@OneToMany(mappedBy = "weatherForecast", cascade=CascadeType.ALL)
	private List<PlaceWeatherForecast> placeWeatherForecasts;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getTempMin() {
		return tempMin;
	}

	public void setTempMin(int tempMin) {
		this.tempMin = tempMin;
	}

	public int getTempMax() {
		return tempMax;
	}

	public void setTempMax(int tempMax) {
		this.tempMax = tempMax;
	}

	public String getPeipsi() {
		return peipsi;
	}

	public void setPeipsi(String peipsi) {
		this.peipsi = peipsi;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Forecast getForecast() {
		return forecast;
	}

	public void setForecast(Forecast forecast) {
		this.forecast = forecast;
	}

	public ForecastTime getForecastTime() {
		return forecastTime;
	}

	public void setForecastTime(ForecastTime forecastTime) {
		this.forecastTime = forecastTime;
	}

	public Phenomenon getPhenomenon() {
		return phenomenon;
	}

	public void setPhenomenon(Phenomenon phenomenon) {
		this.phenomenon = phenomenon;
	}

	public List<PlaceWeatherForecast> getPlaceWeatherForecasts() {
		return placeWeatherForecasts;
	}

	public void setPlaceWeatherForecasts(List<PlaceWeatherForecast> placeWeatherForecasts) {
		this.placeWeatherForecasts = placeWeatherForecasts;
	}

}
