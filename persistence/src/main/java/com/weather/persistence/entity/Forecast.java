package com.weather.persistence.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Forecast {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private Date date;
	@OneToMany(mappedBy = "forecast", cascade=CascadeType.ALL)
	private List<WeatherForecast> weatherForecasts;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<WeatherForecast> getWeatherForecasts() {
		return weatherForecasts;
	}

	public void setWeatherForecasts(List<WeatherForecast> weatherForecasts) {
		this.weatherForecasts = weatherForecasts;
	}
}
