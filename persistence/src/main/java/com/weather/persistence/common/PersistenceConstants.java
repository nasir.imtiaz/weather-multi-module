package com.weather.persistence.common;

public class PersistenceConstants {
	public static final String PLACE_CACHE = "PLACE_CACHE";
	public static final String PHENOMENON_CACHE = "PHENOMENON_CACHE";
	public static final String FORECAST_TIME_CACHE = "FORECAST_TIME_CACHE";
}
