package com.weather.persistence.repository;

import static com.weather.persistence.common.PersistenceConstants.PHENOMENON_CACHE;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weather.persistence.entity.Phenomenon;

@Repository
public interface PhenomenonRepository extends JpaRepository<Phenomenon, Long> {
	@Cacheable(PHENOMENON_CACHE)
	public Phenomenon findByName(String name);
}
