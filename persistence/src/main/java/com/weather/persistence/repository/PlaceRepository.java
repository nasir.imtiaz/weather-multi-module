package com.weather.persistence.repository;

import static com.weather.persistence.common.PersistenceConstants.PLACE_CACHE;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weather.persistence.entity.Place;

@Repository
public interface PlaceRepository extends JpaRepository<Place, Long> {
	@Cacheable(PLACE_CACHE)
	public Place findByName(String name);
}
