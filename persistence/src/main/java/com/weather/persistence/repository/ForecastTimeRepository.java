package com.weather.persistence.repository;

import static com.weather.persistence.common.PersistenceConstants.FORECAST_TIME_CACHE;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weather.persistence.entity.ForecastTime;

@Repository
public interface ForecastTimeRepository extends JpaRepository<ForecastTime, Long> {
	@Cacheable(FORECAST_TIME_CACHE)
	public ForecastTime findByName(String name);
}
