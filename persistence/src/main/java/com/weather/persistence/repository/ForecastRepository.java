package com.weather.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weather.persistence.entity.Forecast;

@Repository
public interface ForecastRepository extends JpaRepository<Forecast, Long> {
}
