package com.weather.domain.common;

import static com.weather.domain.common.DomainConstants.DATE_FORMAT;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.weather.domain.DomainBootApplication;

@Component
public class DomainUtils {
	private static Logger logger = LoggerFactory.getLogger(DomainBootApplication.class);

	public Date parseDate(String date) throws ParseException {
		if (StringUtils.isBlank(date))
			return null;

		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		return dateFormat.parse(date);
	}

}
