package com.weather.domain.common;

import java.nio.charset.Charset;

public class DomainConstants {
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final Charset UTF_8 = Charset.forName("UTF-8");
	public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";
	public static enum FORECAST_TIME {DAY, NIGHT};
}
