package com.weather.domain.mapper;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.weather.domain.common.DomainConstants.FORECAST_TIME;
import com.weather.domain.xml.Day;
import com.weather.persistence.entity.PlaceWeatherForecast;
import com.weather.persistence.entity.WeatherForecast;
import com.weather.persistence.repository.ForecastTimeRepository;
import com.weather.persistence.repository.PhenomenonRepository;

@Component
public class XmlDayToWeatherForecastEntityMapper implements Function<com.weather.domain.xml.Day, WeatherForecast> {
	@Autowired
	ForecastTimeRepository forecastTimeRepository;
	
	@Autowired
	PhenomenonRepository phenomenonRepository;

	@Autowired
	XmlPlaceToPlaceWeatherForecastEntityMapper xmlPlaceToPlaceWeatherForecast;
	
	private static Logger logger = LoggerFactory.getLogger(XmlDayToWeatherForecastEntityMapper.class);

	@Override
	public WeatherForecast apply(Day day) {
		if (day == null)
			return null;

		WeatherForecast weatherForecast = new WeatherForecast();
		weatherForecast.setForecastTime(forecastTimeRepository.findByName(FORECAST_TIME.DAY.toString()));
		weatherForecast.setPeipsi(day.getPeipsi());

		if (StringUtils.isNotBlank(day.getPhenomenon())) {
			weatherForecast.setPhenomenon(phenomenonRepository.findByName(day.getPhenomenon()));

			if (weatherForecast.getPhenomenon() == null) {
				logger.warn(
						"Phenomenon '{}' not found in system. Consult administrator to register the Phenomenon name.",
						day.getPhenomenon());
			}
		}

		if (day.getTempMax() != null) {
			weatherForecast.setTempMax(Integer.valueOf(day.getTempMax()));
		}

		if (day.getTempMin() != null) {
			weatherForecast.setTempMin(Integer.valueOf(day.getTempMin()));
		}

		weatherForecast.setText(day.getText());

		if (day.getPlace() != null && day.getPlace().length > 0) {
			weatherForecast.setPlaceWeatherForecasts(Arrays.asList(day.getPlace()).stream().map(place -> {
				PlaceWeatherForecast placeWeatherForecast = xmlPlaceToPlaceWeatherForecast.apply(place);
				placeWeatherForecast.setWeatherForecast(weatherForecast);
				return placeWeatherForecast;
			}).collect(Collectors.toList()));
		}

		return weatherForecast;
	}

}
