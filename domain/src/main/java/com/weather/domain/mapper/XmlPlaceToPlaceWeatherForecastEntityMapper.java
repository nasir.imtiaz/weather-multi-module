package com.weather.domain.mapper;

import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.weather.domain.xml.Place;
import com.weather.persistence.entity.PlaceWeatherForecast;
import com.weather.persistence.repository.PhenomenonRepository;
import com.weather.persistence.repository.PlaceRepository;

@Component
public class XmlPlaceToPlaceWeatherForecastEntityMapper implements Function<com.weather.domain.xml.Place, PlaceWeatherForecast> {
	@Autowired
	PlaceRepository placeRepository;
	
	@Autowired
	PhenomenonRepository phenomenonRepository;
	
	private static Logger logger = LoggerFactory.getLogger(XmlPlaceToPlaceWeatherForecastEntityMapper.class);
	
	@Override
	public PlaceWeatherForecast apply(Place place) {
		if (place == null)
			return null;

		PlaceWeatherForecast placeWeatherForecast = new PlaceWeatherForecast();

		if (StringUtils.isNotBlank(place.getName())) {
			placeWeatherForecast.setPlace(placeRepository.findByName(place.getName()));

			if (placeWeatherForecast.getPlace() == null) {
				logger.warn("Place '{}' not found in system. Consult administrator to register the place name.",
						place.getName());
			}
		}

		if (StringUtils.isNotBlank(place.getPhenomenon())) {
			placeWeatherForecast.setPhenomenon(phenomenonRepository.findByName(place.getPhenomenon()));

			if (placeWeatherForecast.getPhenomenon() == null) {
				logger.warn(
						"Phenomenon '{}' not found in system. Consult administrator to register the Phenomenon name.",
						place.getPhenomenon());
			}
		}

		if (StringUtils.isNotBlank(place.getTempMin())) {
			placeWeatherForecast.setTempMin(Integer.valueOf(place.getTempMin()));
		}

		return placeWeatherForecast;
	}

}
