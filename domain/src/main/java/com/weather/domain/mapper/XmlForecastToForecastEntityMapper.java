package com.weather.domain.mapper;

import java.text.ParseException;
import java.util.Arrays;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.weather.domain.common.DomainUtils;
import com.weather.persistence.entity.Forecast;
import com.weather.persistence.entity.WeatherForecast;

@Component
public class XmlForecastToForecastEntityMapper implements Function<com.weather.domain.xml.Forecast, com.weather.persistence.entity.Forecast>{
	@Autowired
	DomainUtils domainUtils;
	
	@Autowired
	XmlDayToWeatherForecastEntityMapper xmlDayToWeatherForecastMapper;
	
	@Autowired
	XmlNightToWeatherForecastEntityMapper xmlNightToWeatherForecastMapper;
	
	private static Logger logger = LoggerFactory.getLogger(XmlForecastToForecastEntityMapper.class);

	@Override
	public com.weather.persistence.entity.Forecast apply(com.weather.domain.xml.Forecast forecastXml) {
		if (forecastXml == null)
			return null;

		com.weather.persistence.entity.Forecast forecast = new Forecast();

		try {
			forecast.setDate(domainUtils.parseDate(forecastXml.getDate()));
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			return null;
		}

		WeatherForecast dayWeatherForecast = xmlDayToWeatherForecastMapper.apply(forecastXml.getDay());
		dayWeatherForecast.setForecast(forecast);

		WeatherForecast nightWeatherForecast = xmlNightToWeatherForecastMapper.apply(forecastXml.getNight());
		nightWeatherForecast.setForecast(forecast);

		forecast.setWeatherForecasts(Arrays.asList(dayWeatherForecast, nightWeatherForecast));

		return forecast;
	}

}
