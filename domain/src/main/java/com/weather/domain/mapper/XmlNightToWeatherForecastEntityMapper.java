package com.weather.domain.mapper;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.weather.domain.common.DomainConstants.FORECAST_TIME;
import com.weather.domain.xml.Night;
import com.weather.persistence.entity.PlaceWeatherForecast;
import com.weather.persistence.entity.WeatherForecast;
import com.weather.persistence.repository.ForecastTimeRepository;
import com.weather.persistence.repository.PhenomenonRepository;

@Component
public class XmlNightToWeatherForecastEntityMapper implements Function<com.weather.domain.xml.Night, WeatherForecast> {
	@Autowired
	ForecastTimeRepository forecastTimeRepository;
	
	@Autowired
	PhenomenonRepository phenomenonRepository;
	
	@Autowired
	XmlPlaceToPlaceWeatherForecastEntityMapper xmlPlaceToPlaceWeatherForecast;
	
	private static Logger logger = LoggerFactory.getLogger(XmlNightToWeatherForecastEntityMapper.class);

	@Override
	public WeatherForecast apply(Night night) {
		if (night == null)
			return null;

		WeatherForecast weatherForecast = new WeatherForecast();
		weatherForecast.setForecastTime(forecastTimeRepository.findByName(FORECAST_TIME.NIGHT.toString()));
		weatherForecast.setPeipsi(night.getPeipsi());

		if (StringUtils.isNotBlank(night.getPhenomenon())) {
			weatherForecast.setPhenomenon(phenomenonRepository.findByName(night.getPhenomenon()));

			if (weatherForecast.getPhenomenon() == null) {
				logger.warn(
						"Phenomenon '{}' not found in system. Consult administrator to register the Phenomenon name.",
						night.getPhenomenon());
			}
		}

		if (StringUtils.isNotBlank(night.getTempMax())) {
			weatherForecast.setTempMax(Integer.valueOf(night.getTempMax()));
		}

		if (StringUtils.isNotBlank(night.getTempMin())) {
			weatherForecast.setTempMin(Integer.valueOf(night.getTempMin()));
		}

		weatherForecast.setText(night.getText());

		if (night.getPlace() != null && night.getPlace().length > 0) {
			weatherForecast.setPlaceWeatherForecasts(Arrays.asList(night.getPlace()).stream().map(place -> {
				PlaceWeatherForecast placeWeatherForecast = xmlPlaceToPlaceWeatherForecast.apply(place);
				placeWeatherForecast.setWeatherForecast(weatherForecast);
				return placeWeatherForecast;
			}).collect(Collectors.toList()));
		}

		return weatherForecast;
	}

}
