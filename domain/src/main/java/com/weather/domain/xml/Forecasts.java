package com.weather.domain.xml;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Forecasts {
	private Forecast[] forecast;

	@XmlElement
	public Forecast[] getForecast() {
		return forecast;
	}

	public void setForecast(Forecast[] forecast) {
		this.forecast = forecast;
	}

	@Override
	public String toString() {
		return "Forecasts [forecast=" + Arrays.toString(forecast) + "]";
	}
}