package com.weather.domain.xml;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlElement;

public class Day {
	private String phenomenon;

	private String tempMax;

	private String tempMin;

	private String peipsi;

	private String text;

	private Place[] place;

	@XmlElement
	public String getPhenomenon() {
		return phenomenon;
	}

	public void setPhenomenon(String phenomenon) {
		this.phenomenon = phenomenon;
	}

	@XmlElement
	public String getPeipsi() {
		return peipsi;
	}

	public void setPeipsi(String peipsi) {
		this.peipsi = peipsi;
	}

	@XmlElement
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@XmlElement(name="tempmax")
	public String getTempMax() {
		return tempMax;
	}

	public void setTempMax(String tempMax) {
		this.tempMax = tempMax;
	}

	@XmlElement(name="tempmin")
	public String getTempMin() {
		return tempMin;
	}

	public void setTempMin(String tempMin) {
		this.tempMin = tempMin;
	}

	@XmlElement
	public Place[] getPlace() {
		return place;
	}

	public void setPlace(Place[] place) {
		this.place = place;
	}

	@Override
	public String toString() {
		return "Day [phenomenon=" + phenomenon + ", tempMax=" + tempMax + ", tempMin=" + tempMin + ", peipsi="
				+ peipsi + ", text=" + text + ", place=" + Arrays.toString(place) + "]";
	}
}
