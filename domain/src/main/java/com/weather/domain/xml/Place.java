package com.weather.domain.xml;

import javax.xml.bind.annotation.XmlElement;

public class Place {
	private String phenomenon;

	private String tempMin;

	private String name;

	@XmlElement
	public String getPhenomenon() {
		return phenomenon;
	}

	public void setPhenomenon(String phenomenon) {
		this.phenomenon = phenomenon;
	}

	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "tempmin")
	public String getTempMin() {
		return tempMin;
	}

	public void setTempMin(String tempMin) {
		this.tempMin = tempMin;
	}

	@Override
	public String toString() {
		return "Place [phenomenon=" + phenomenon + ", tempMin=" + tempMin + ", name=" + name + "]";
	}
}
