package com.weather.domain.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Forecast {

	private Night night;

	private Day day;

	private String date;

	@XmlElement
	public Night getNight() {
		return night;
	}

	public void setNight(Night night) {
		this.night = night;
	}

	@XmlElement
	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	@XmlAttribute
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Forecast [night=" + night + ", day=" + day + ", date=" + date + "]";
	}
}
