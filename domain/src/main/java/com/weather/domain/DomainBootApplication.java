package com.weather.domain;

import static com.weather.domain.common.DomainConstants.USER_AGENT;
import static com.weather.domain.common.DomainConstants.UTF_8;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.weather.domain.mapper.XmlForecastToForecastEntityMapper;
import com.weather.domain.xml.Forecast;
import com.weather.domain.xml.Forecasts;
import com.weather.persistence.repository.ForecastRepository;

@Component
public class DomainBootApplication {
	private static Logger logger = LoggerFactory.getLogger(DomainBootApplication.class);
	@Autowired
	private XmlForecastToForecastEntityMapper xmlForecastToForecastEntityMapper;
	@Autowired
	private ForecastRepository forecastRepository;
	@Value("${weather.forecast.poll.url}")
	private String baseUrl;

	@Scheduled(fixedDelayString = "${weather.forecast.poll.frequency.millis}")
	public void pollWeatherInfo() {
		logger.info("Polling weather forecast from URL '{}' @ [{}]", baseUrl, new Date());

		ResponseEntity<Forecasts> response = sendWeatherPollRequest();

		if (HttpStatus.OK == response.getStatusCode()) {
			storeWeatherInfo(response.getBody().getForecast());
		}
	}

	private ResponseEntity<Forecasts> sendWeatherPollRequest() {
		HttpEntity<String> request = prepareWeatherPollRequestEntity();

		RestTemplate restClient = new RestTemplate();
		restClient.getMessageConverters().add(0, new StringHttpMessageConverter(UTF_8));

		ResponseEntity<Forecasts> response = restClient.exchange(baseUrl, HttpMethod.GET, request, Forecasts.class);

		logger.info("Polling response code: {}", response.getStatusCode());
		logger.info(
				"Polling result: \n=================================================\n{}\n=================================================",
				response.getBody());

		return response;
	}

	private HttpEntity<String> prepareWeatherPollRequestEntity() {
		HttpHeaders headers = prepareWeatherPollRequestHeader();
		HttpEntity<String> request = new HttpEntity<>(headers);
		return request;
	}

	private HttpHeaders prepareWeatherPollRequestHeader() {
		HttpHeaders headers = new HttpHeaders();
		MediaType mediaType = new MediaType(MediaType.APPLICATION_XML, UTF_8);
		headers.setAccept(Arrays.asList(new MediaType[] { mediaType }));
		headers.add("user-agent", USER_AGENT);
		headers.setAcceptCharset(Arrays.asList(UTF_8));

		return headers;
	}

	private void storeWeatherInfo(Forecast[] forecastArr) {
		logger.info("Storing weather forecast...");

		if (forecastArr == null)
			return;

		List<Forecast> xmlForecasts = Arrays.asList(forecastArr);

		List<com.weather.persistence.entity.Forecast> forecasts = xmlForecasts.stream()
				.map(xmlForecastToForecastEntityMapper).collect(Collectors.toList());

		logger.info("Discarding old weather forecast data");
		forecastRepository.deleteAll(); // Discarding old forecast data

		logger.info("Storing latest weather forecast data");
		forecastRepository.saveAll(forecasts);

		logger.info("Weather forecast stored successfully");
	}

}
