package com.weather.web.dto;

import java.util.Date;

public class ForecastDto {
	private Date date;
	private String forecastTime;
	private String phenomenon;
	private int tempMin;
	private int tempMax;
	private String text;
	private String peipsi;
	private String placePhenomenon;
	private String place;
	private Integer placeTempMin;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getForecastTime() {
		return forecastTime;
	}

	public void setForecastTime(String forecastTime) {
		this.forecastTime = forecastTime;
	}

	public String getPhenomenon() {
		return phenomenon;
	}

	public void setPhenomenon(String phenomenon) {
		this.phenomenon = phenomenon;
	}

	public int getTempMin() {
		return tempMin;
	}

	public void setTempMin(int tempMin) {
		this.tempMin = tempMin;
	}

	public int getTempMax() {
		return tempMax;
	}

	public void setTempMax(int tempMax) {
		this.tempMax = tempMax;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPeipsi() {
		return peipsi;
	}

	public void setPeipsi(String peipsi) {
		this.peipsi = peipsi;
	}

	public String getPlacePhenomenon() {
		return placePhenomenon;
	}

	public void setPlacePhenomenon(String placePhenomenon) {
		this.placePhenomenon = placePhenomenon;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Integer getPlaceTempMin() {
		return placeTempMin;
	}

	public void setPlaceTempMin(Integer placeTempMin) {
		this.placeTempMin = placeTempMin;
	}
}
