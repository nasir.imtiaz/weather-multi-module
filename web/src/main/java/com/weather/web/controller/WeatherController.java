package com.weather.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weather.web.dto.ForecastDto;
import com.weather.web.service.WeatherService;

@RestController
public class WeatherController {
	@Autowired
	private WeatherService weatherService;
	
	@RequestMapping("/forecasts")
	public List<ForecastDto> getWeatherForecast() {
		return weatherService.getWeatherForecast();
	}
	
}
