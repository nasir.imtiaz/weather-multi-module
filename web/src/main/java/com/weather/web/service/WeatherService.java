package com.weather.web.service;

import java.util.List;

import com.weather.web.dto.ForecastDto;

public interface WeatherService {
	public List<ForecastDto> getWeatherForecast();
}
