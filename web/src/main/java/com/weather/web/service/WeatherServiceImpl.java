package com.weather.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.weather.persistence.entity.Forecast;
import com.weather.persistence.entity.PlaceWeatherForecast;
import com.weather.persistence.entity.WeatherForecast;
import com.weather.persistence.repository.ForecastRepository;
import com.weather.web.common.WebUtils;
import com.weather.web.dto.ForecastDto;

@Service
public class WeatherServiceImpl implements WeatherService {
	@Autowired
	private ForecastRepository forecastRepository;
	@Autowired
	private WebUtils webUtils;

	@Override
	public List<ForecastDto> getWeatherForecast() {
		List<Forecast> forecastList = forecastRepository.findAll();
		List<ForecastDto> forecastDtoList = new ArrayList<>();

		for (Forecast forecast : forecastList) {
			for (WeatherForecast weatherForecast : forecast.getWeatherForecasts()) {
				List<PlaceWeatherForecast> placeWeatherForecasts = weatherForecast.getPlaceWeatherForecasts();

				if (CollectionUtils.isEmpty(placeWeatherForecasts)) {
					forecastDtoList.add(webUtils.weatherForecastToForecastDto.apply(weatherForecast));
				} else {
					forecastDtoList.addAll(placeWeatherForecasts.stream()
							.map(webUtils.placeWeatherForecastToForecastDto).collect(Collectors.toList()));
				}
			}
		}

		return forecastDtoList;
	}

}
