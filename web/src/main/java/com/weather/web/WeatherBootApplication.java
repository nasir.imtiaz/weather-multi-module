package com.weather.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@PropertySource(value = {"persistence-application.properties", "domain-application.properties"})
@EnableJpaRepositories(basePackages = {"com.weather.persistence"})
@EntityScan(basePackages = {"com.weather.persistence.entity"})
@ComponentScan(basePackages = {"com.weather"})
@EnableScheduling
@EnableCaching
public class WeatherBootApplication {
	public static void main(String[] args) {
		SpringApplication.run(WeatherBootApplication.class);
	}
}
