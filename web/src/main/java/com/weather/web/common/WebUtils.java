package com.weather.web.common;

import java.util.function.Function;

import org.springframework.stereotype.Component;

import com.weather.persistence.entity.PlaceWeatherForecast;
import com.weather.persistence.entity.WeatherForecast;
import com.weather.web.dto.ForecastDto;

@Component
public class WebUtils {

	public Function<PlaceWeatherForecast, ForecastDto> placeWeatherForecastToForecastDto = new Function<PlaceWeatherForecast, ForecastDto>() {
		@Override
		public ForecastDto apply(PlaceWeatherForecast placeWeatherForecast) {
			if (placeWeatherForecast == null || placeWeatherForecast.getWeatherForecast() == null || placeWeatherForecast.getWeatherForecast().getForecast() == null)
				return null;

			ForecastDto forecastDto = new ForecastDto();
			forecastDto.setDate(placeWeatherForecast.getWeatherForecast().getForecast().getDate());
			forecastDto.setForecastTime(placeWeatherForecast.getWeatherForecast().getForecastTime().getName());
			forecastDto.setPhenomenon(placeWeatherForecast.getWeatherForecast().getPhenomenon().getName());
			forecastDto.setTempMin(placeWeatherForecast.getWeatherForecast().getTempMin());
			forecastDto.setTempMax(placeWeatherForecast.getWeatherForecast().getTempMax());
			forecastDto.setText(placeWeatherForecast.getWeatherForecast().getText());
			forecastDto.setPeipsi(placeWeatherForecast.getWeatherForecast().getPeipsi());
			
			if (placeWeatherForecast.getPlace() != null) {
				forecastDto.setPlace(placeWeatherForecast.getPlace().getName());
				forecastDto.setPlacePhenomenon(placeWeatherForecast.getPhenomenon().getName());
				forecastDto.setPlaceTempMin(placeWeatherForecast.getTempMin());
			}

			return forecastDto;
		}
	};

	public Function<WeatherForecast, ForecastDto> weatherForecastToForecastDto = new Function<WeatherForecast, ForecastDto>() {
		@Override
		public ForecastDto apply(WeatherForecast weatherForecast) {
			if (weatherForecast == null || weatherForecast.getForecast() == null)
				return null;

			ForecastDto forecastDto = new ForecastDto();
			forecastDto.setDate(weatherForecast.getForecast().getDate());
			forecastDto.setForecastTime(weatherForecast.getForecastTime().getName());
			forecastDto.setPhenomenon(weatherForecast.getPhenomenon().getName());
			forecastDto.setTempMin(weatherForecast.getTempMin());
			forecastDto.setTempMax(weatherForecast.getTempMax());
			forecastDto.setText(weatherForecast.getText());
			forecastDto.setPeipsi(weatherForecast.getPeipsi());

			return forecastDto;
		}
	};
}
